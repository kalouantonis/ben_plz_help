# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :ben_plz_help, BenPlzHelpWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "cKR/d7x5sJlfK6d9iCl/sPmDbjjZV3GacX6DEyTCu96X4aMuF9PNjc8ALumM8Y1j",
  render_errors: [view: BenPlzHelpWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: BenPlzHelp.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "VLWnOtAs"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
