defmodule BenPlzHelpWeb.Router do
  use BenPlzHelpWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api" do
    pipe_through :api

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: BenPlzHelpWeb.GraphQL.Schema,
      interface: :playground

    forward "/", Absinthe.Plug,
      schema: BenPlzHelpWeb.GraphQL.Schema
  end
end
