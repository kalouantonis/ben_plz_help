defmodule BenPlzHelpWeb.GraphQL do
  use Absinthe.Schema.Notation

  import_sdl """
  type FirstQueries {
    randomInteger(signed: Boolean): Int!
  }

  type SecondQueries {
    greeting(special: Boolean): String!
  }
  """

  def random_integer(_params, _info) do
    {:ok, 4} # guaranteed random
  end

  def greeting(_params, _info) do
    {:ok, "Hello there, whoever you may be."}
  end
end

defmodule BenPlzHelpWeb.GraphQL.FirstHydration do
  def hydrate do
    %{
      first_queries: %{
        random_integer: [
          resolve: &BenPlzHelpWeb.GraphQL.random_integer/2
        ],
      }
    }
  end
end

defmodule BenPlzHelpWeb.GraphQL.SecondHydration do
  def hydrate do
    %{
      second_queries: %{
        greeting: [
          resolve: &BenPlzHelpWeb.GraphQL.greeting/2,
        ]
      }
    }
  end
end

defmodule BenPlzHelpWeb.GraphQL.Schema do
  use Absinthe.Schema

  import_types BenPlzHelpWeb.GraphQL

  query do
    import_fields :first_queries
    import_fields :second_queries
  end

  def hydrate(%Absinthe.Blueprint{}, _) do
    hydrators = [
      &BenPlzHelpWeb.GraphQL.FirstHydration.hydrate/0,
      &BenPlzHelpWeb.GraphQL.SecondHydration.hydrate/0,
    ]

    Enum.reduce(hydrators, %{}, fn hydrate_fn, hydrated ->
      hydrate_merge(hydrated, hydrate_fn.())
    end)
  end

  # hydrations fallback
  def hydrate(_node, _ancestors) do
    []
  end

  defp hydrate_merge(a, b) do
    Map.merge(a, b, fn _, a, b -> Map.merge(a, b) end)
  end
end
